from __future__ import annotations

import logging

from rich.logging import RichHandler


def setup_logger(
    logging_level: int = 0, set_warn: list[str] | None = None
) -> None:
    if set_warn is None:
        set_warn = []

    log = logging.getLogger()

    ch = RichHandler()

    formatter = logging.Formatter(
        "%(message)s",
        "%Y-%m-%d %H:%M:%S",
    )
    ch.setFormatter(formatter)
    log.addHandler(ch)

    if logging_level >= 2:
        log.setLevel(logging.DEBUG)
        ch.setLevel(logging.DEBUG)
    elif logging_level >= 1:
        log.setLevel(logging.INFO)
        log.setLevel(logging.INFO)
    else:
        log.setLevel(logging.WARNING)
        log.setLevel(logging.WARNING)

    for warn in set_warn:
        logging.getLogger(warn).setLevel(logging.WARNING)
