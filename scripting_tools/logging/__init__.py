from ._logging import setup_logger  # noqa: F401

setup_logger.__module__ = __name__


__all__ = ["setup_logger"]
