"""
Implementation of Perona-Malik PDE solution for time series smoothing.

As written in https://arxiv.org/pdf/1412.6291.pdf
and implemented in https://wire.insiderfinance.io/preserving-edges-when-smoothening-time-series-data-90f9d965132e
"""  # noqa: E501
from __future__ import annotations

import numpy as np
import numpy.typing as npt
import scipy.signal as signal
from numba import njit, prange

__all__ = ["convolve_PDE", "perona_malik_smooth", "butter_lowpass_filter"]


@njit
def convolve_PDE(
    U: npt.NDArray[np.float64 | np.float32], sigma: float = 1, k: float = 0.05
) -> npt.NDArray[np.float64]:
    """
    Perform Gaussian convolution by solving the heat equation with Neumann
    boundary conditions

    :param U: The array to perform convolution on.
    :param sigma: The standard deviation of the gaussian convolution.
    :param k: The step-size for the finite difference scheme (keep < 0.1 for accuracy)

    :return: The convolved function
    """  # noqa: E501

    t_end = sigma**2 / 2

    factor = 1 - 2 * k

    C = U.copy().astype(np.float64)
    for _ in prange(int(t_end / k)):
        # Implementing the neumann boundary conditions
        C[0] = 2 * k * C[1] + factor * C[0]
        C[-1] = 2 * k * C[-2] + factor * C[-1]

        # Scheme on the interior nodes
        C[1:-1] = k * (C[2:] + C[:-2]) + factor * C[1:-1]

    return C


@njit
def gradient1(x: np.ndarray) -> np.ndarray:
    """
    Calculate the first order gradient for an array, excluding the edge values.

    :param x: array of length N

    :return: array of length N-2
    """
    return (x[2:] - x[:-2]) / 2


@njit
def gradient2(x: np.ndarray) -> np.ndarray:
    """
    Calculate the second order gradient for an array, excluding the edge values.

    :param x: array of length N

    :return: array of length N-2
    """  # noqa: E501
    return x[2:] - 2 * x[1:-1] + x[:-2]


@njit(parallel=True, nogil=True, cache=True)
def perona_malik_smooth(
    p: npt.NDArray[np.float32 | np.float64],
    alpha: float = 50.0,
    k: float = 0.05,
    t_end: float = 5.0,
) -> npt.NDArray[np.float64]:
    """
    Solve the Gaussian convolved Perona-Malik PDE using a basic finite
    difference scheme.

    Total number of iteration steps: t_end / k

    Parameters
    ----------
    p : np.array
        The signal to smoothen.
    alpha : float, optional
        A parameter to control how much the PDE resembles the heat equation,
        the perona malik PDE -> heat equation as alpha -> infinity
    k : float, optional
        The step size in time (keep < 0.1 for accuracy)
    t_end : float, optional
        When to termininate the algorithm the larger the t_end, the smoother
        the series
    Returns
    -------
    U : np.array=
        The Perona-Malik smoothened time series
    """

    U = p.astype(np.float64)

    for _ in prange(int(t_end / k)):
        # Find the convolution of U with the gaussian, this ensures that the
        # PDE problem is well posed
        C = convolve_PDE(U, k=k)

        # Determine the derivatives by using matrix multiplication
        Cx = gradient1(C)
        Cxx = gradient2(C)

        Ux = gradient1(U)
        Uxx = gradient2(U)

        # Find the spatial component of the PDE
        PDE_space = (
            alpha * Uxx / (alpha + Cx**2)
            - 2 * alpha * Ux * Cx * Cxx / (alpha + Cx**2) ** 2
        )

        # Solve the PDE for the next time-step
        U[1:-1] += k * PDE_space

    return U


def butter_lowpass(
    cutoff: float, fs: float, order: int = 5
) -> tuple[float, float]:
    """
    :param cutoff: The cutoff frequency of the filter.
    :param fs: The sampling frequency of the signal.
    :param order: The order of the filter.

    """
    return signal.butter(order, cutoff, fs=fs, btype="low", analog=False)


def butter_lowpass_filter(
    data: np.ndarray, cutoff: int, fs: float, order: int
) -> np.ndarray:
    """
    :param data: The data to filter.
    :param cutoff: The cutoff frequency of the filter.
    :param fs: The sampling frequency of the signal.
    :param order: The order of the filter.
    """
    b, a = butter_lowpass(cutoff, fs, order)
    y = signal.filtfilt(b, a, data)
    return y
